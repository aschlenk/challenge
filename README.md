# IllumioChallenge


This code base contains the code for the firewall class described in the Illumio challenge homework. 

The Firewall class is included in the src/models package which contains all code related to building and constructing a firewall object with a set of rules given form an input file.



## Design Choices

The code uses a HashMap in order to store the created rules for easy lookup when checking to see if a packet should be accepted, and thus, routed to the desired destination. 
This choice was made to reduce the latency associated with checking a packet against the set of potential rules for a firewall. 
In this case, the rule is hashed base on its characteristics and stored in the HashMap in a position that hash an ArrayList or the rules associated with that hash. 
This gives a constant lookup time to find the associated rules for a given packet and allows only a subset of the rules to be checked for each check_packet call. 


It was useful to create a class for a Rule which stores the related information about the direction, protocol, port and ip address. 
It takes as input a line (expected from the given csv file) and parses the input line to get the corresponding information to create the rule. 
This allows for a cleaner, seperated module where the Rule class is unlinked from the Firewall class. 


The test file is given in the package src/tests and was used to create the initial instance of the Firewall class and to test the basic functionality. 
As we expect correct inputs, this file does not check for errors in the associated csv files and the check_packet commands. 
Given more time, it would be useful to instrument several unit tests and error checking in the firewall class. 



### Improving the Performance

Currently, the hash function is not implemented in such a way to ensure there are not many collisions in the HashMap. 
This is due to a time constraint as I was looking for a clean solution that would provide 1) an easy way to store Rules without many collisions (non constant lookup time increasing latency), and 2) a hash that can be computed for an incoming packet to find the associated rules. 
If given more time, I would work to optimize this hash to ensure both of the performance measures are balanced and optimized. 


# Interested Teams

I would be greatly interested in working with the Policy team as much of my PhD research focused on developing backend policy algorithms for various security domains. 
Secondarily, I would be very interested in the data team as they have quite an inportant task in scaling the ability to analyze the data from networks. 
