package models;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Firewall {
	
	private String ruleFile;
	private Map<Integer, Rule> firewallRules;
	
	public Firewall(String filename) throws FileNotFoundException{
		this.ruleFile = filename;
		firewallRules = new HashMap<>();
		readInRules();
	}
	
	public boolean accept_packet(String direction, String protocol, int port, String ip){
		//hash function needs to be done in such a way that I can easily find the asscoiated rules for an incoming packet
		int hash = computeHash(direction, protocol, port, ip); 
		
		//Grab Rules associated with the hash
		Rule r1 = firewallRules.get(hash);
		
		if(r1 == null) //no value returned for the hash value meaning no corresponding rules to check against
			return false; 
			
		if(passRule(r1, direction, protocol, port, ip))
			return true;
		
		return false;
	}

	private boolean passRule(Rule r, String direction, String protocol, int port, String ip){
		if(!r.dir.equals(direction))
			return false;
		
		if(!r.protocol.equals(protocol))
			return false;
		
		if(port < r.lowPort || port > r.highPort)
			return false;
		
		if(!checkIPValue(r, ip))	
			return false;
		
		//passed all checks
		return true;
	}
	
	
	/**
	 * Checks if a given ip address is within range for a particular rule. 
	 * @param r
	 * @param ip
	 * @return
	 */
	private boolean checkIPValue(Rule r, String ip){
		int [] ipvalue = new int[4];
		Scanner sc1 = new Scanner(ip).useDelimiter("\\.");
		ipvalue[0] = Integer.parseInt(sc1.next());
		ipvalue[1] = Integer.parseInt(sc1.next());
		ipvalue[2] = Integer.parseInt(sc1.next());
		ipvalue[3] = Integer.parseInt(sc1.next());
		
		if(ipvalue[0] < r.lowIP[0] || ipvalue[0] > r.highIP[0])
			return false;
		else if(ipvalue[1] < r.lowIP[1] || ipvalue[1] > r.highIP[1])
			return false;
		else if(ipvalue[2] < r.lowIP[2] || ipvalue[2] > r.highIP[2])
			return false;
		else if(ipvalue[3] < r.lowIP[3] || ipvalue[3] > r.highIP[3])
			return false;
		
		return true;
	}
	
	private void readInRules() throws FileNotFoundException{
		Scanner sc = new Scanner(new File(ruleFile));
		
		//Expects a csv file with direction, protocol, port, IP address
		while(sc.hasNextLine()){
			String line = sc.nextLine();
			Rule r = new Rule(line);
			insertRule(r);
		}
		
	}
	
	/**
	 * Insert rule in hashtable for all ports and ip addresses in-between the lower and upper bound. 
	 * @param r
	 */
	private void insertRule(Rule r){
		for(int port = r.lowPort; port <= r.highPort; port++){
			//Iterate through all possible IP addresses for a given rule
			for(int ip0=r.lowIP[0]; ip0 <= r.highIP[0]; ip0++){
				for(int ip1=r.lowIP[1]; ip1 <= r.highIP[1]; ip1++){
					for(int ip2=r.lowIP[2]; ip2 <= r.highIP[2]; ip2++){
						for(int ip3=r.lowIP[3]; ip3 <= r.highIP[3]; ip3++){
							//Compute the hash to ensure this year is referenced for all requests which fall in its range
							String ip = ip0+"."+ip1+"."+ip2+"."+ip3;
							int hash = computeHash(r.dir, r.protocol, port, ip);
							firewallRules.put(hash, r);
						}
					}	
				}
			}
				
		}
	}
	
	private class Rule{
		
		protected String dir;
		protected String protocol;
		protected int lowPort;
		protected int highPort;
		protected int [] lowIP;
		protected int [] highIP;
		protected int hash;
		
		protected Rule(String line){
			parseInputLine(line);
		}
		
		/**
		 * parse input line for the arguments to construct a rule
		 * @param line
		 */
		private void parseInputLine(String line){
			Scanner sc = new Scanner(line).useDelimiter(",");
			
			dir = sc.next();
			protocol = sc.next();
			getPorts(sc.next());
			getIPs(sc.next());
//			computeHash();
		}
		
		/**
		 * Initializes the IP addresses and reads in the low and high IP address values for a rule.
		 * @param ips
		 */
		private void getIPs(String ips){
			lowIP = new int[4];
			highIP = new int[4];
			
			if(ips.contains("-")){
				Scanner sc = new Scanner(ips).useDelimiter("-");
				setIP(lowIP, sc.next());
				setIP(highIP, sc.next());
			}else{
				setIP(lowIP, ips);
				setIP(highIP, ips);
			}
		}
		
		private void setIP(int [] ip, String ips){
			Scanner sc1 = new Scanner(ips).useDelimiter("\\.");
			ip[0] = Integer.parseInt(sc1.next());
			ip[1] = Integer.parseInt(sc1.next());
			ip[2] = Integer.parseInt(sc1.next());
			ip[3] = Integer.parseInt(sc1.next());
		}
		
		/**
		 * Read in the low and high ports for a given rule. 
		 * @param ports
		 */
		private void getPorts(String ports){
			if(ports.contains("-")){
				Scanner sc = new Scanner(ports).useDelimiter("-");
				lowPort = Integer.parseInt(sc.next());
				highPort = Integer.parseInt(sc.next());
			}else{
				lowPort = Integer.parseInt(ports);
				highPort = lowPort;
			}
			
		}
		
//		public void computeHash(){
//			//String ip = lowIP[0]+"."+lowIP[1]+"."+lowIP[2]+"."+lowIP[3];
//			String varstr = dir + protocol;// + lowPort + ip;
//			hash = varstr.hashCode();
//		}
				
		public String toString(){
			return dir+" : "+protocol+" : "+lowPort+"-"+highPort+" : "+
					lowIP[0]+"."+lowIP[1]+"."+lowIP[2]+"."+lowIP[3]+"-"+highIP[0]+"."+highIP[1]+"."+highIP[2]+"."+highIP[3];
		}
	}
	
	/**
	 * Hashcode method for a rule.  
	 * TODO: Improve the overall performance to reduce the number of collisions possible in the hashmap.
	 * @param direction
	 * @param protocol
	 * @param port
	 * @param ip
	 * @return
	 */
	public static int computeHash(String direction, String protocol, int port, String ip){
		String varstr = direction + protocol + port + ip;
		int  hc = varstr.hashCode();
		return hc;
	}
	
}
