package tests;

import java.io.FileNotFoundException;

import models.Firewall;

public class test1 {
	
	public static void main (String [] args) throws FileNotFoundException{
		//Test creation of firewall object
		Firewall fw = new Firewall("rules.csv");
		
		System.out.println(fw.accept_packet("inbound", "tcp", 80, "192.168.1.2")); // should match first rule
		System.out.println(fw.accept_packet("inbound", "udp", 53, "192.168.2.1")); //  matches third rule
		System.out.println(fw.accept_packet("outbound", "tcp", 10234, "192.168.10.11")); // matches second rule
		System.out.println(fw.accept_packet("inbound", "tcp", 81, "192.168.1.2")); //does not match a rule
		System.out.println(fw.accept_packet("inbound", "udp", 24, "52.12.48.92")); //does not match a rule
		
	}
}
